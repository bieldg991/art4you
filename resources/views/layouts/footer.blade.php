<footer class="footer-distributed d-block" style="position: absolute;">

    <div class="footer-left">

        <img src="{{asset('img/logooficial.png')}}" alt="" style="width:130px;heigth:25px;">

        <p class="footer-links">
            <a href="/obras" class="link-1">Obras</a>

            <a href="/artistas">Artistas</a>

            <a href="/contacto">Contacto</a>

            <a href="/nosotros">Nosotros</a>

            <a href="/carro">Carro</a>

            <a href="#">Cookies</a>
        </p>

        <p class="footer-company-name">Art4You CopyRight © 2020</p>
    </div>

    <div class="footer-center">
        <div>
            <i class="fa fa-map-marker"></i>
            <p><span>Vic</span> 08500 </p>
        </div>
        <div>
            <i class="fa fa-phone"></i>
            <p>+34 633811729</p>
        </div>
        <div>
            <i class="fa fa-envelope"></i>
            <p><a href="mailto:bieldg99@gmail.com">bieldg99@gmail.com</a></p>
        </div>
    </div>
    <div class="footer-right">
        <p class="footer-company-about">
            <span>About Art4You</span>
            Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
        </p>
        <div class="footer-icons">

            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-github"></i></a>
        </div>
    </div>
</footer>